Most recent updates appear at the top of this file.

KNOWN BUGS
==========
<NONE>


version 001 / 000
-----------------
-DEPENDENCIES: MultiCODE v539.045, MQTT-C (C:\Home\Programmation\SourcesC\Downloads\paho.mqtt.c-master\install), and MQTT-C++ MQTT-C (C:\Home\Programmation\SourcesC\Downloads\paho.mqtt.cpp-master\install).
-TESTED: basic test code validated.


version 000 / 000
-----------------
-DEPENDENCIES: MultiCODE v538.045, MQTT-C (C:\Home\Programmation\SourcesC\Downloads\paho.mqtt.c-master\install), and MQTT-C++ MQTT-C (C:\Home\Programmation\SourcesC\Downloads\paho.mqtt.cpp-master\install).
-CREATION: first commit.
