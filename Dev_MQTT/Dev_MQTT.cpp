#include <stdlib.h>
#include <string.h>
#include <WinSock2.h>

#include <mqtt/topic.h>
#include <mqtt/client.h>
#include <mqtt/async_client.h>
#include <MQTTClient.h>

#pragma comment(lib,"Ws2_32.Lib")
#pragma comment(lib,"paho-mqtt3a-static.lib")
#pragma comment(lib,"paho-mqtt3c-static.lib")
#pragma comment(lib,"paho-mqttpp3-static.lib")

using namespace mqtt;

#define ADDRESS     "10.150.1.112:1883"
#define CLIENTID    "LYUABE"

void publish(MQTTClient client, char* topic, char* payload)
{
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	pubmsg.payload = payload;
	pubmsg.payloadlen = strlen((char*)pubmsg.payload);
	pubmsg.qos = 2;
	pubmsg.retained = 0;
	MQTTClient_deliveryToken token;
	MQTTClient_publishMessage(client, topic, &pubmsg, &token);
	MQTTClient_waitForCompletion(client, token, 1000L);
	printf("Message '%s' with delivery token %d delivered\n", payload, token);
}

int on_message(void* context, char* topicName, int topicLen, MQTTClient_message* message)
{
	char* payload = (char*)message->payload;

	printf("Received operation [%s] %s\n", topicName, payload);
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}

void on_connection_lost(void* context, char* cause)
{
	printf("\nConnection lost\n");
	printf("     cause: %s\n", cause);
}

int main(int argc, char* argv[])
{
	//mqtt::string url = "10.150.1.100:1883";
	//mqtt::string clientID = "JOVIAL/temperatures";
	//mqtt::string pdir = "";

	//mqtt::client cli(url, clientID, NULL);
	//return 0;

	MQTTClient client;
	MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	conn_opts.username = "universcity";
	conn_opts.password = "UniversCity2103";

	MQTTClient_setCallbacks(client, NULL, on_connection_lost, on_message, NULL);

	int rc;
	if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
	{
		printf("Failed to connect, return code %d\n", rc);
		exit(-1);
	}
	////create device
	//publish(client, "s/us", "100,C MQTT,c8y_MQTTDevice");
	////set hardware information
	//publish(client, "s/us", "110,S123456789,MQTT test model,Rev0.1");
	////listen for operation
	//MQTTClient_subscribe(client, "s/ds", 0);

	int QOS = 1;

	//mqtt_publish("Mount/coder/alt", ALT);
	MQTTClient_subscribe(client, "Mount/coder/alt", QOS);
	MQTTClient_subscribe(client, "Mount/coder/az", QOS);
	MQTTClient_subscribe(client, "Dome/sensors/temp", QOS);
	MQTTClient_subscribe(client, "Dome/sensors/humidity", QOS);
	MQTTClient_subscribe(client, "MDome/sensors/dewpoint", QOS);

	//mqtt_publish("Mount/coder/az", AZ);
	//mqtt_publish("Dome/sensors/temp", temp);
	//mqtt_publish("Dome/sensors/humidity", humidity);
	//mqtt_publish("Dome/sensors/dewpoint", dewpoint);
	//mqtt_publish("test", temp);

	for (;;)
	{

		Sleep(3);
	}



	MQTTClient_disconnect(client, 1000);
	MQTTClient_destroy(&client);
	system("pause");

	return rc;

}